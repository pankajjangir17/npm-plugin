function tripInformation(){
    return {
        "tripCountry":"Chicago",
        "tripCity": "Lake View Condo",
        "tripAddress":"1232 N Main St",
        "tripCheckInDate":"28-Aug-2018",
        "tripCheckInTime":"04:00 PM",
        "tripCheckoutDate":"04-Sep-2018",
        "tripCheckoutTime":"11:00 AM",
        "eSignDocumentRequired":true,
        "eSignStatus":"Done"
    }
}