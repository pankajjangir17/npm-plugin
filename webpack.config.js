const webpack = require('webpack')
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const path = require('path');
// const SRC_DIR = path.resolve(__dirname,'src');
// const DIST_DIR = path.resolve(__dirname, 'dist');
module.exports = {
  devtool: 'source-map',
  entry: './src/index.js',
  output: {
    path: __dirname + "/dist",
    //publicPath: "/src/public/",
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets:[ 'es2015', 'react', 'stage-2' ]
        }
      },{
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: "css-loader"
        })

      },{
        test: /\.(jpg|png)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 60000,
          },
        },
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
        'process.env':{
            'NODE_ENV':JSON.stringify("production")
        }
    }),
    new ExtractTextPlugin('/css/tripInformation.css')
  ]
};