import React from 'react'
import {connect} from 'react-redux'
import '../src/public/css/tripInformation.css'
import weatherIcon from '../src/public/images/mytripsfor-newdevelopment-download@2x.png'
import checkOutIcon from '../src/public/images/mytripsfor-newdevelopment-group-12@2x.png'
import lineSeprator from '../src/public/images/mytripsfor-newdevelopment-line-2.png'
import ovelIcon from '../src/public/images/mytripsfor-newdevelopment-oval-3@2x.png'
import backgroundTextureImage from '../src/public/images/mytripsfor-newdevelopment-rectangle-5.png'
import backgroundImage from '../src/public/images/mytripsfor-newdevelopment-usa-2016mg1638.png'

class TripInformationComponent extends React.Component {
  handleClick() {
    console.log('this is:', this);
  }
  render() {
    var tripData = this.props.tripData
    return (
      <div class="mytripsfornewdevelopment">
        <div style={{ "width": 1440, "height": "100%", "position": "relative", "margin": "auto" }}>
          <div class="rectangle4">
          </div>
          <div className="group7">
            <div className="group4">
              <div className="rectangle4copy">
              </div>
              <img anima-src={weatherIcon} className="download" src={weatherIcon} />
              <img anima-src={backgroundImage} className="usa2016mg1638" src={backgroundImage} />
              <img anima-src={backgroundTextureImage} className="rectangle5" src={backgroundTextureImage} />
              <div className="chicagoil">
              {tripData.tripCountry}
              </div>
              <img anima-src={lineSeprator} className="line2" src={lineSeprator} />
              <div className="a80">
                80
              </div>
              <img anima-src={ovelIcon} className="oval3" src={ovelIcon} />
              <div className="thursday1220pm">
                {tripData.tripCity}
              </div>
              <div className="aug28sept4">
              {tripData.tripCountry}
              </div>
              <div className="lakeviewcondo">
              {tripData.tripCity}
              </div>
              <div className="a1234nmainst">
              {tripData.tripCountry}
              </div>
              <div className="checkin200pm">
                <span className="span1">Check In:</span><span className="span2">{tripData.tripCountry}</span>
                <button onClick={(e) => this.handleClick(e)} className="btn btn-square btn-lg btn-filled-green">Register</button>
              </div>
              <div className="checkout1145pm">
                <span className="span1">Check Out:</span><span className="span2">{tripData.tripCountry}</span>
              </div>
              <div className="group8copy">
                <div className="rectangle">
                </div>
                <div className="viewalltrips">
                  View All Trips
                </div>
                <div className="label1">
                  &gt;
                </div>
              </div>
            </div>            
            </div>
            
          
        </div></div>
    )
  }
}

const mapStateToProps = (state, ownProps) =>{
  return{        
        tripData: state.tripInformationReducer === undefined ? '' : (state.tripInformationReducer.tripData === undefined ? '' : state.tripInformationReducer.tripData)
      }
  }    

const mapDispatchToProps = (dispatch, ownProps) => {
  return {      
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(TripInformationComponent)