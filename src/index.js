import React from 'react'
import ReactDOM from 'react-dom'
import configureStore from '../src/redux/store/configureStore'
//import { hashHistory} from 'react-router'
//import { syncHistoryWithStore } from 'react-router-redux'
import Root from '../src/redux/containers/Root'

const store = configureStore()
//const history = syncHistoryWithStore(hashHistory, store)
ReactDOM.render(
    <Root store={store}/>,
    document.getElementById("root")
);