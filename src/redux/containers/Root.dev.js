import React from 'react'
import PropTypes from 'prop-types';
import { Provider } from 'react-redux'
import App from '../containers/App.jsx'
import DevTools from './DevTools'

const Root = ({ store }) => (
  <Provider store={store}>
    <div>
      <App />
      <DevTools />
    </div>
  </Provider>
)

Root.propTypes = {
  store: PropTypes.object.isRequired  
}

export default Root
