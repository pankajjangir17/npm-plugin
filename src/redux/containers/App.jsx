import React, { Component } from 'react'
import TripInformationComponent from '../../TripInformationComponent';
import { connect } from 'react-redux'
import {fetchTripInformationData} from '../middlewares/tripInformationMiddleware'

class App extends Component {
  constructor(props) {
    super(props);    
  }

  componentDidMount() {
    const { userId, tripId} = this.props;        
    this.refreshTripData = setInterval(()=> {
      console.log("fetching API");
      this.props.fetchTripInformation(userId, tripId);
    }, 1000);
    this.props.fetchTripInformation(userId, tripId)
  }
  componentWillUnmount() {
    window.clearInterval(this.refreshTripData);    
}
  render() {
    return (
      <div class="mytripsfornewdevelopment">
        <div style={{ "width": 1440, "height": "100%", "position": "relative", "margin": "auto" }}>
          <div class="rectangle4">
          </div>
          <TripInformationComponent/>          
        </div></div>
    )
  }
}
const mapStateToProps=(state) => ({}) 

const mapDispatchToProps=(dispatch) =>({
  fetchTripInformation:(userId, tripId)=>{
    dispatch(fetchTripInformationData(userId, tripId))
  }  
})
export default connect(mapStateToProps, mapDispatchToProps)(App)