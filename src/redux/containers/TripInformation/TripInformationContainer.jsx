import React, { Component } from 'react'
import { connect } from 'react-redux'
import TripInformationComponent from '../../../TripInformationComponent'

class TripInformationContainer extends Component {
  render() {
    return (
      <div class="mytripsfornewdevelopment">
        <div style={{ "width": 1440, "height": "100%", "position": "relative", "margin": "auto" }}>
          <div class="rectangle4">
          </div>
          <TripInformationComponent tripData={this.props.tripData} />          
        </div></div>
    )
  }
}
const mapStateToProps = (state, ownProps) => ({
  tripData: state.tripInfoReducer === undefined ? '' : state.tripInfoReducer.tripData === undefined ? '' : state.tripInfoReducer.tripData
})
const mapDispatchToProps = (dispatch, ownProps) => ({
})
export default connect(mapStateToProps, mapDispatchToProps)(TripInformationContainer)