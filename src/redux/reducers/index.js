//import { reducer as formReducer } from 'redux-form'
import tripInformationReducer from './tripInformationReducer'
import {routerReducer } from 'react-router-redux'
import {combineReducers} from 'redux'

export const rootReducer = combineReducers({
 //form: formReducer,
 routing: routerReducer,
 tripInformationReducer
 });
export default rootReducer


