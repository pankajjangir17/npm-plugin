import {setTripData} from '../actions/tripInformationAction'
import axios from 'axios';

export function fetchTripInformationData(userId, tripId) {
    return (dispatch, getState) => {
        var state = getState();
        
        var data =  {
            "tripCountry":"Chicago",
            "tripCity": "Lake View Condo",
            "tripAddress":"1232 N Main St",
            "tripCheckInDate":"28-Aug-2018",
            "tripCheckInTime":"04:00 PM",
            "tripCheckoutDate":"04-Sep-2018",
            "tripCheckoutTime":"11:00 AM",
            "eSignDocumentRequired":true,
            "eSignStatus":"Done"
        }
        var tripData = data; //window.tripInformation()
        dispatch(setTripData(tripData))
        // axios.get('http://demo7916700.mockable.io/tripInformation')
        //     .then(function (response) {
        //         if(response.status = 200){
        //             dispatch(setTripData(tripData))
        //         }                
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });        
    }
}