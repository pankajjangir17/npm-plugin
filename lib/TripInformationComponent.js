'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

require('../src/public/css/tripInformation.css');

var _mytripsforNewdevelopmentDownload2x = require('../src/public/images/mytripsfor-newdevelopment-download@2x.png');

var _mytripsforNewdevelopmentDownload2x2 = _interopRequireDefault(_mytripsforNewdevelopmentDownload2x);

var _mytripsforNewdevelopmentGroup122x = require('../src/public/images/mytripsfor-newdevelopment-group-12@2x.png');

var _mytripsforNewdevelopmentGroup122x2 = _interopRequireDefault(_mytripsforNewdevelopmentGroup122x);

var _mytripsforNewdevelopmentLine = require('../src/public/images/mytripsfor-newdevelopment-line-2.png');

var _mytripsforNewdevelopmentLine2 = _interopRequireDefault(_mytripsforNewdevelopmentLine);

var _mytripsforNewdevelopmentOval32x = require('../src/public/images/mytripsfor-newdevelopment-oval-3@2x.png');

var _mytripsforNewdevelopmentOval32x2 = _interopRequireDefault(_mytripsforNewdevelopmentOval32x);

var _mytripsforNewdevelopmentRectangle = require('../src/public/images/mytripsfor-newdevelopment-rectangle-5.png');

var _mytripsforNewdevelopmentRectangle2 = _interopRequireDefault(_mytripsforNewdevelopmentRectangle);

var _mytripsforNewdevelopmentUsa2016mg = require('../src/public/images/mytripsfor-newdevelopment-usa-2016mg1638.png');

var _mytripsforNewdevelopmentUsa2016mg2 = _interopRequireDefault(_mytripsforNewdevelopmentUsa2016mg);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var TripInformationComponent = function (_React$Component) {
  _inherits(TripInformationComponent, _React$Component);

  function TripInformationComponent() {
    _classCallCheck(this, TripInformationComponent);

    return _possibleConstructorReturn(this, (TripInformationComponent.__proto__ || Object.getPrototypeOf(TripInformationComponent)).apply(this, arguments));
  }

  _createClass(TripInformationComponent, [{
    key: 'handleClick',
    value: function handleClick() {
      console.log('this is:', this);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var tripData = this.props.tripData;
      return _react2.default.createElement(
        'div',
        { 'class': 'mytripsfornewdevelopment' },
        _react2.default.createElement(
          'div',
          { style: { "width": 1440, "height": "100%", "position": "relative", "margin": "auto" } },
          _react2.default.createElement('div', { 'class': 'rectangle4' }),
          _react2.default.createElement(
            'div',
            { className: 'group7' },
            _react2.default.createElement(
              'div',
              { className: 'group4' },
              _react2.default.createElement('div', { className: 'rectangle4copy' }),
              _react2.default.createElement('img', { 'anima-src': _mytripsforNewdevelopmentDownload2x2.default, className: 'download', src: _mytripsforNewdevelopmentDownload2x2.default }),
              _react2.default.createElement('img', { 'anima-src': _mytripsforNewdevelopmentUsa2016mg2.default, className: 'usa2016mg1638', src: _mytripsforNewdevelopmentUsa2016mg2.default }),
              _react2.default.createElement('img', { 'anima-src': _mytripsforNewdevelopmentRectangle2.default, className: 'rectangle5', src: _mytripsforNewdevelopmentRectangle2.default }),
              _react2.default.createElement(
                'div',
                { className: 'chicagoil' },
                tripData.tripCountry
              ),
              _react2.default.createElement('img', { 'anima-src': _mytripsforNewdevelopmentLine2.default, className: 'line2', src: _mytripsforNewdevelopmentLine2.default }),
              _react2.default.createElement(
                'div',
                { className: 'a80' },
                '80'
              ),
              _react2.default.createElement('img', { 'anima-src': _mytripsforNewdevelopmentOval32x2.default, className: 'oval3', src: _mytripsforNewdevelopmentOval32x2.default }),
              _react2.default.createElement(
                'div',
                { className: 'thursday1220pm' },
                tripData.tripCity
              ),
              _react2.default.createElement(
                'div',
                { className: 'aug28sept4' },
                tripData.tripCountry
              ),
              _react2.default.createElement(
                'div',
                { className: 'lakeviewcondo' },
                tripData.tripCity
              ),
              _react2.default.createElement(
                'div',
                { className: 'a1234nmainst' },
                tripData.tripCountry
              ),
              _react2.default.createElement(
                'div',
                { className: 'checkin200pm' },
                _react2.default.createElement(
                  'span',
                  { className: 'span1' },
                  'Check In:'
                ),
                _react2.default.createElement(
                  'span',
                  { className: 'span2' },
                  tripData.tripCountry
                ),
                _react2.default.createElement(
                  'button',
                  { onClick: function onClick(e) {
                      return _this2.handleClick(e);
                    }, className: 'btn btn-square btn-lg btn-filled-green' },
                  'Register'
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'checkout1145pm' },
                _react2.default.createElement(
                  'span',
                  { className: 'span1' },
                  'Check Out:'
                ),
                _react2.default.createElement(
                  'span',
                  { className: 'span2' },
                  tripData.tripCountry
                )
              ),
              _react2.default.createElement(
                'div',
                { className: 'group8copy' },
                _react2.default.createElement('div', { className: 'rectangle' }),
                _react2.default.createElement(
                  'div',
                  { className: 'viewalltrips' },
                  'View All Trips'
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'label1' },
                  '>'
                )
              )
            )
          )
        )
      );
    }
  }]);

  return TripInformationComponent;
}(_react2.default.Component);

var mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    tripData: state.tripInformationReducer === undefined ? '' : state.tripInformationReducer.tripData === undefined ? '' : state.tripInformationReducer.tripData
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch, ownProps) {
  return {};
};
exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(TripInformationComponent);
